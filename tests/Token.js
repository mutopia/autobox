define([
  "doh/runner",
  "dojo/has",
  "require",
  // Code under test
  "autobox/Token"
], function(doh, has, require, Token) {

  var TEST_ID = "123";
  var TEST_LABEL = "Test label";

  // Created in setup.
  var testToken;

  function setup() {
    // summary:
    //      Create a populated Token object to test.
    testToken = new Token({
      itemId: TEST_ID,
      text: TEST_LABEL
    });
  }

  function tearDown() {
    // summary:
    //      Destroy the created Token object.
    testToken = null;
  }

  doh.register("autobox/tests/Token", {
    testCreate: function() {
      doh.assertEqual(TEST_LABEL, testToken.get("text"));
      doh.assertEqual(TEST_ID, testToken.get("itemId"));
    },

    testText: function() {
      doh.assertEqual(TEST_LABEL, testToken.get("text"));
      testToken.set("text", TEST_ID);
      doh.assertEqual(TEST_ID, testToken.get("text"));
    },

    testValue: function() {
      doh.assertEqual("$" + TEST_ID, testToken.get("value"));
      testToken.set("itemId", TEST_LABEL);
      doh.assertEqual("$" + TEST_LABEL, testToken.get("value"));
    }
  }, setup, tearDown);

  // Test the Token element in the browser.
  if (has("host-browser")) {
    doh.register("autobox/tests/TokenHtml", require.toUrl("./Token.html"), {async: 0});
  }
});
