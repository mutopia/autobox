define([
  "doh/runner",
  "dojo/aspect",
  // Code under test
  "autobox/AutoInput",
  // Support
  "dojo/store/Memory"
], function(doh, aspect, AutoInput, Memory) {
  var TEST_STORE = new Memory({data: [
    {id: "AMD", name: "Advanced Micro Devices"},
    {id: "AMZN", name: "Amazon"},
    {id: "APPL", name: "Apple"},
    {id: "GOOG", name: "Google"},
    {id: "INTC", name: "Intel"},
    {id: "MSFT", name: "Microsoft"},
    {id: "NVDA", name: "NVIDIA"},
    {id: "QCOM", name: "Qualcomm"},
    {id: "YHOO", name: "Yahoo!"}
  ]});
  var TEST_ITEM = {id: "GOOG", name: "Google"};
  var TEST_QUERY = {};

  var TEST_VALUE = "1+1";

  var testInput;

  function setup() {
    // summary:
    //      Create a populated AutoInput object to test.
    testInput = new AutoInput({
      name: "state",
      store: TEST_STORE,
      query: TEST_QUERY,
      searchAttr: "name",
      autoComplete: true
    });
  }

  function tearDown() {
    // summary:
    //      Destroy the created Token object.
    testInput = null;
  }

  doh.register("autobox/tests/AutoInput::creation", {
    testCreate_empty: function() {
      // summary:
      //      Tests that an AutoInput cannot be created with no constructor arguments.
      try {
        var input = new AutoInput();
      } catch (e) {
        if (e !== "You must provide a store to create an AutoInput.") {
          throw "AutoInput failed to report constructor store requirement."
        }
      }
    },

    testCreate_noStore: function() {
      // summary:
      //      Tests that an AutoInput can be created without a store or query.
      try {
        var input = new AutoInput({
          name: "state",
          searchAttr: "name",
          autoComplete: true
        });
      } catch (e) {
        if (e !== "You must provide a store to create an AutoInput.") {
          throw "AutoInput failed to report constructor store requirement."
        }
      }
    }
  });

  doh.register("autobox/tests/AutoInput", {

    testValue: function() {
      doh.assertEqual("", testInput.get("value"));
      testInput.set("value", TEST_VALUE);
      doh.assertEqual(TEST_VALUE, testInput.get("value"));
      testInput.set("value", "");
      doh.assertEqual("", testInput.get("value"));

      // Remove the input value and test that the placeholder value is accepted.
      testInput.set("placeholder", TEST_VALUE);
      testInput.usePlaceholderAsValue = true;
      doh.assertEqual(TEST_VALUE, testInput.get("value"));
    },

    testIsValid: function() {
      var validValues = [
        TEST_VALUE,
        "",
        "(1+2)-(3*(4/5))",
        "((1+1)+(1+(1+1))+1)",
        "1 + 2  *   3 "
      ];
      var invalidValues = [
        "a",
        "1+a+1",
        "var",
        "GOOG",
        "Google"
        // TODO(orlade): Implement bracket matching.
//                "(1+1))",
//                "((1+1)",
//                "1+((1)+1)+1+(1+1))+1"
      ];
      validValues.forEach(function(value) {
        testInput.set("value", value);
        doh.assertTrue(testInput.isValid(), value + " should be valid.");
      });
      invalidValues.forEach(function(value) {
        testInput.set("value", value);
        doh.assertFalse(testInput.isValid(), value + " should be invalid.");
      });
    },

    testMatchToken: function() {
      // summary:
      //      Tests that tokens are matched or not matched correctly based on the current input state.
      var nonmatchingValues = [
        "",
        "GOOG",
        {id: "ABCD", name: "Google"}
      ];
      var handle = aspect.before(testInput, "tokenise", function(item, fullText, start, end) {
        throw "Incorrectly matched: " + item + " " + fullText + " " + start + " " + end;
      });
      nonmatchingValues.forEach(function(value) {
        testInput.currentItem = value;
        testInput.matchToken(); // No content, no match.
      });
      handle.remove();

      // Record correct matches, then throw an error if not all were matched correctly.
      var matchingValues = [
        TEST_ITEM,
        // Matching is based only on ID.
        {id: "GOOG", name: "Microsoft"}
      ];
      var matched = [];
      handle = aspect.before(testInput, "tokenise", function(item, fullText, start, end) {
        matched.push([item, fullText, start, end]);
      });

      matchingValues.forEach(function(value) {
        testInput.currentItem = value;
        testInput.matchToken(); // No content, no match.
      });
      handle.remove();

      if (matched.length !== matchingValues.length) {
        throw "Missed some matches; only matched: " + matched;
      }
    },

    testSearch: function() {
      // summary:
      //      Tests that the input correctly searches for matches in the store.
      var searchKeys = {
        "a": ["AMD", "AMZN", "APPL"],
        "am": ["AMD", "AMZN"],
        "amazo": ["AMZN"],
        "z": [],
        // Empty string matches all items.
        "": TEST_STORE.data.map(function(item) {
          return item.id;
        })
      };
      var dfd = new doh.Deferred();

      // Intercept search results to check against expectations.
      aspect.before(testInput, "onSearch", function(results, query, options) {
        var key = query["name"].toString().replace("*", "");
        doh.assertEqual(searchKeys[key].length, results.length);
        for (var i = 0; i < results.length; i++) {
          doh.assertEqual(searchKeys[key][i], results[i].id);
        }
        dfd.callback(true);
      });

      // Set the input value to each key and start the search.
      Object.keys(searchKeys).forEach(function(key) {
        testInput.set("value", key);
        testInput.setCaretPos(1);
        testInput._startSearchFromInput();
      });
      return dfd;
    }
  }, setup, tearDown);
});
