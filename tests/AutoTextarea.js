define([
  "doh/runner",
  "dojo/has",
  "require",
  // Code under test
  "autobox/AutoTextarea",
  // Support
  "dojo/store/Memory"
], function(doh, has, require, AutoTextarea, Memory) {
  var store = new Memory({data: [
    {id: "AMD", name: "Advanced Micro Devices"},
    {id: "AMZN", name: "Amazon"},
    {id: "APPL", name: "Apple"},
    {id: "GOOG", name: "Google"},
    {id: "INTC", name: "Intel"},
    {id: "MSFT", name: "Microsoft"},
    {id: "NVDA", name: "NVIDIA"},
    {id: "QCOM", name: "Qualcomm"},
    {id: "YHOO", name: "Yahoo!"}
  ]});

  var TEST_VALUE = "1 + $GOOG - 1";

  doh.register("autobox/tests/AutoTextarea::creation", [
    function testCreate_empty() {
      new AutoTextarea();
    },
    function testCreate_store() {
      var area = new AutoTextarea({store: store});
      doh.assertEqual(area.store, store);
    }
  ]);

  // testArea: AutoTextarea
  //      An AutoTextarea instance to test.
  var testArea;

  function setup() {
    // summary:
    //      Create a populated Token object to test.
    testArea = new AutoTextarea({store: store});
  }

  function tearDown() {
    // summary:
    //      Destroy the created Token object.
    testArea = null;
  }

  doh.register("autobox/tests/AutoTextarea", {
    testValue: function() {
      // summary:
      //      Tests that getting a value with a token returns the original value.
      doh.assertEqual(null, testArea.get("value"));
      testArea.set("value", TEST_VALUE);
      doh.assertEqual(TEST_VALUE, testArea.get("value"));
      testArea.set("value", null);
      doh.assertEqual(null, testArea.get("value"));
    }
  }, setup, tearDown);

  // Test the AutoTextarea element in the browser.
  if (has("host-browser")) {
    doh.register("autobox/tests/AutoTextareaHtml", require.toUrl("./AutoTextarea.html"));
  }
});
