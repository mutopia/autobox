define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/aspect',
  'dojo/on',
  'dojo/query',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/focus',
  'dojo/store/Memory',
  'autobox/Token',
  'dijit/form/_TextBoxMixin',
  'dijit/form/FilteringSelect',
  // Plugins
  'dojo/NodeList-manipulate'
], function(declare, lang, aspect, on, query, domConstruct, domAttr, focusUtils, Memory, Token, _TextBoxMixin, FilteringSelect) {
  return declare([FilteringSelect], {

    // summary:
    //      Inspired by http://www.codeproject.com/Articles/183498/Fancy-Facebook-Style-TextboxList

    // baseClass: String
    //      The CSS class to apply to the widget's DOM node.
    baseClass: "autoboxInput",

    // required: Boolean
    //      Whether or not a non-empty value is required for this field to be valid.
    required: false,

    // usePlaceholderAsValue: Boolean
    //      Whether or not the placeholder value should be accepted as the input's value if no input is given.
    usePlaceholderAsValue: false,

    // tooltipPosition: String[]
    //      The positions in which tooltips may appear, in decreasing order of preference.
    tooltipPosition: ["below", "above"],

    // pattern: String
    //      Very simple regex string for mathematical expressions
    //      TODO(orlade); Implement stronger mathematical validation
    pattern: "[\\s\\d\\+\\-\\*/\\^\\.\\(\\)]*",

    // selectionEnd: int
    //      The index at which the selection of the input text ends.
    selectionEnd: 0,

    constructor: function(/*Object*/ params) {
      if (!params || !params.store) {
        throw "You must provide a store to create an AutoInput.";
      }
      this.inherited(arguments);
    },

    postCreate: function() {
      this.inherited(arguments);
      // For some reason just setting hasDownArrow: false above doesn't work
      this.set("hasDownArrow", false);

      this.own(
          // Catch CLICK or ENTER on selected dropdown item
          aspect.after(this, "_selectOption", this.matchToken),
          // Catch ENTER when a dropdown item is not selected
          aspect.after(this, "_setBlurValue", lang.hitch(this, function() {
            this.get("value") && this.matchToken();
          }))
      );
    },

    _startSearchFromInput: function() {
      // summary:
      //      Override the logic of initiating search from input to use only the nearest
      //      valid (alphanumeric) characters to match items.

      // Look back to the start of the current valid string
      var cpos = this._getCaretPos(this.focusNode);
      this.substart = cpos;
      while (this.substart > 0 && this.focusNode.value.charAt(this.substart - 1).match(/[\w\d]/)) {
        this.substart--;
      }

      var key = this.focusNode.value.substring(this.substart, cpos);
      this._startSearch(key);

      // Validate to flag invalid input immediately
      this.validate();
    },

    _announceOption: function(/*Node*/ node) {
      if (!node) {
        return;
      }
      // pull the text value from the item attached to the DOM node
      // But don't set anything yet
      var newValue;
      if (node == this.dropDown.nextButton ||
          node == this.dropDown.previousButton) {
        newValue = node.innerHTML;
      } else {
        var item = this.dropDown.items[node.getAttribute("item")];
        this.currentItem = item;
        newValue = (this.store._oldAPI ? // remove getValue() for 2.0 (old dojo.data API)
            this.store.getValue(item, this.searchAttr) : item[this.searchAttr]).toString();
      }
      var prefix = this.focusNode.value.substring(0, this.substart);
      // set up ARIA activedescendant
      this.focusNode.setAttribute("aria-activedescendant", domAttr.get(node, "id"));
      // autocomplete the rest of the option to announce change
      this._autoCompleteText(newValue);
    },

    _autoCompleteText: function(/*String*/ text) {
      var fn = this.focusNode;
      var cpos = this._getCaretPos(fn);

      // Keep the leading and trailing text
      var left = fn.value.substring(0, this.substart);
      // Find the next valid character, and remove all preceding characters
      // TODO Think of a more robust way to do this
      var endIndex = cpos;
      while (endIndex < fn.value.length && !this.validator(fn.value.charAt(endIndex), this.constraints)) {
        endIndex++;
      }
      var right = fn.value.substr(endIndex);

      fn.value = left + text.substr(0, 3) + right;

      _TextBoxMixin.selectInputText(fn, cpos, this.substart + text.length);
    },

    isValid: function(/*Boolean*/ /*===== isFocused =====*/) {
      // summary:
      //      Overridden to check only the substring in question
      return this.validator(this.focusNode.value.substr(this.substart), this.constraints);
    },

//        Override to debug validation
//        validator: function (/*anything*/ value, /*__Constraints*/ constraints) {
//            // summary:
//            //      Overridden to
//            var result = this.inherited(arguments);
//            return result;
//        },

    matchToken: function() {
      // summary:
      //      Using the input's current state, attempt to identify a complete token.

      // If there is a current item and it's a match in the backing store
      if (this.currentItem && (!this.store || this.store.get(this.currentItem.id))) {
        // Call the tokenise hook
        this.tokenise(this.currentItem, this.focusNode.value, this.substart, this.substart + this.currentItem.name.length);
        // Reset for a new token input
        this.substart = undefined;
        this.currentItem = null;
      }
    },

    tokenise: function(item, fullText, start, end) {
      // summary:
      //      Custom event for the owner of this input to handle tokenisation.
      //      Users should listen with aspect.after(autoInput, "tokenise", handlerFunc).
      //      Arguments are the string to tokenise, and its start and end indexes.
    },

    getCaretPos: function() {
      // summary:
      //      Shortcut to get the position of the caret in the textbox.
      return this._getCaretPos(this.focusNode);
    },

    setCaretPos: function(index) {
      // summary:
      //      Shortcut to set the position of the caret in the textbox.
      // index:
      //      The numeric character index to place the caret before (from [0, length]).
      //      May also be one of the strings {"start", "end"}.
      if (index === "start") {
        index = 0;
      } else if (index === "end") {
        index = this.focusNode.value.length;
      }
      _TextBoxMixin._setSelectionRange(this.focusNode, index, index);
    },

    _getValueAttr: function() {
      // summary:
      //      Get the current value of the input. If there is no input, use the placeholder, if any.
      var val = this.focusNode.value;
      if (!val && this.usePlaceholderAsValue) {
        val = domAttr.get(this.focusNode, "placeholder");
      }
      if (!val) {
        val = '';
      }
      return val;
    },

    _setValueAttr: function(value) {
      // summary:
      //      Set the current value of the input.
      this.focusNode.value = value;
    },

    _setPlaceholderAttr: function(text) {
      domAttr.set(this.focusNode, "placeholder", text);
    }
  });
});
