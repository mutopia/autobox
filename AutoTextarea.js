define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/event',
  'dojo/keys',
  'dojo/on',
  'dojo/when',
  'dojo/aspect',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-geometry',
  'dijit/focus',
  'dojo/store/Memory',
  'autobox/Token',
  'dijit/form/TextBox',
  'autobox/AutoInput',
  'dijit/form/_TextBoxMixin',
  'dijit/layout/ContentPane',
  'dijit/_CssStateMixin'
], function(declare, lang, array, event, keys, on, when, aspect, domConstruct, domClass, domStyle, domGeometry, focusUtils, Memory, Token, TextBox, AutoInput, _TextBoxMixin, ContentPane, _CssStateMixin) {
  // Mix in _CssStateMixin to apply textbox-like styles on events (such as hover)
  return declare([ContentPane, _CssStateMixin], {

    // summary:
    //      An autocompleting widget which splices token objects in between FilteringSelect widgets.

    // Add dijitTextBox so the _CssStateMixin handles all the animations
    "class": "autobox autoboxTextarea",
    baseClass: "dijitTextBox",

    // Cached sum of the widths of the tokens to simplify resizing
    tokensWidth: 0,
    // The regex use to match an item reference.
    itemRegex: null,

    // The datastore backing the autocomplete fields
    store: null,
    // The query to pass with all store requests
    query: null,

    // The value that is in the process of being set asynchronously, if any.
    pending: null,

    constructor: function() {
      this.query = {};
      this.itemRegex = /\$(\d+)/gmi;
      // Define internal array
      // Ordered list of the input field widgets
      this.inputWidgets = [];
      // Ordered list of the token widget
      this.tokens = [];
    },

    postCreate: function() {
      this.inherited(arguments);

      // A store is required to create child inputs, so if one wasn't passed in the constructor, use a placeholder
      if (!this.store) {
        this.store = new Memory({});
      }

      // Place the initial input field
      var newInput = this.buildInput();
      this.inputWidgets.push(newInput);
      domConstruct.place(newInput.domNode, this.containerNode, "last");

      // After creating or deleting a token, resize the children to fit the widget
      this.own(
          aspect.after(this, "tokenise", this.fitLayout),
          aspect.after(this, "onTokenDelete", this.fitLayout)
      );
//            aspect.after(this, "onChange", this.fitLayout);
    },

    startup: function() {
      // summary:
      //      Sets the widget up after creation, relative to its surrounding elements.
      this.resize();
      // Resize again in case the parent hasn't finished drawing yet (e.g. if embedded in a dgrid).
      setTimeout(lang.hitch(this, this.resize), 0);
    },

    resize: function() {
      // summary:
      //      Sets the initial size of the widget once it is created.

      // Store the size properties of this container widget
      var dimensions = this._calculateDimensions();
      this.containerWidth = dimensions.w;
      this.containerHeight = dimensions.h;
      this.fitLayout();
    },

    _calculateDimensions: function() {
      // summary:
      //      Calculates the dimensions of the AutoTextarea to determine the space available for children.
      var containerStyle = domStyle.getComputedStyle(this.containerNode);
      // FIXME for some reason, this is giving -2 for height and width...
      return domGeometry.getContentBox(this.containerNode, containerStyle);
    },

    _onInputKeydown: function(input, evt) {
      // summary:
      //      Handle a key press within one of the input fields
      //      If one of the interesting conditions matches, the event is stopped, since all
      //      widget-related behaviour is fully specified here.
      var widgetIndex = array.indexOf(this.inputWidgets, input);
      var inputLength = input.get("value").length;
      var key = evt.charCode || evt.keyCode;
      switch (key) {
        case keys.LEFT_ARROW:
          if (widgetIndex > 0 && input.getCaretPos() == 0) {
            // Jump to the previous widget
            this.focusInput(this.inputWidgets[widgetIndex - 1], "end");
            event.stop(evt);
          }
          break;
        case keys.RIGHT_ARROW:
          if (widgetIndex < this.inputWidgets.length - 1 && input.getCaretPos() == inputLength) {
            // Jump to the next widget
            this.focusInput(this.inputWidgets[widgetIndex + 1], "start");
            event.stop(evt);
          }
          break;
        case keys.BACKSPACE:
          if (widgetIndex > 0 && input.getCaretPos() == 0) {
            var token = this.tokens[widgetIndex - 1];
            if (token) { // Delete the previous token
              token.remove();
            } else { // Partial merge into the previous input
              var lastInput = this.inputWidgets[widgetIndex - 1],
                  lastValue = lastInput.get("value");
              if (input.get("value")) {
                var firstChar = input.get("value") ? input.get("value")[0] : "";
                lastInput.set("value", lastValue.substr(0, lastValue.length - 1) + firstChar);
                input.set("value", input.get("value").substr(1));
                this.focusInput(lastInput, lastValue.length - 1);
              } else {
                lastInput.set("value", lastValue.substr(0, lastValue.length - 1));
                this.removeInput(input);
              }
            }
            // Merging the inputs will be taken care of in onTokenDelete
            event.stop(evt);
          }
          break;
        case keys.DELETE:
          if (widgetIndex < this.inputWidgets.length - 1 && input.getCaretPos() == inputLength) {
            var token = this.tokens[widgetIndex];
            if (token) { // Delete the next token
              token.remove();
            } else { // Partial merge from the next input
              var nextInput = this.inputWidgets[widgetIndex + 1],
                  nextValue = nextInput.get("value");
              if (nextValue) {
                nextInput.set("value", nextValue.substr(1));
              } else {
                this.removeInput(nextInput);
                this.tokens.splice(widgetIndex, 1);
              }
            }
            // Merging the inputs will be taken care of in onTokenDelete
            event.stop(evt);
          }
          break;
        case keys.HOME:
          this.focusInput(this.inputWidgets[0], "start");
          event.stop(evt);
          break;
        case keys.END:
          this.focusInput(this.inputWidgets[this.inputWidgets.length - 1], "end");
          event.stop(evt);
          break;
        case keys.TAB:
          break;
        default:
          // No match, no action, but also no event stopping
          return;
      }
    },

    buildInput: function(/*String*/ text, /*Boolean?*/ append) {
      // summary:
      //      Construct a new input field with the necessary properties and bindings.
      //      The caller is required to store the reference and DOM node unless append is truthy.
      // text: String
      //      The value to set on the input.
      // append: Boolean
      //      Whether to append the new input to the input container automatically (for convenience).
      text = text || null;
      // Create the widget
      // Note: This AutoTextarea must be attached to the DOM before creating child inputs
      var newInput = new AutoInput({
        name: "state",
        store: this.store,
        query: this.query,
        searchAttr: "name",
        "class": "autoboxInput",
        autoComplete: true
      });
      // Ensure that the textarea's query is used as necessary
      lang.delegate(newInput.query, this.query);
      if (text) {
        newInput.set("value", text);
      }

      newInput.own(
          // Resize on when the contents of the input change
          // TODO Bind to a more explicit event for keystrokes
          on(newInput.focusNode, "input", lang.hitch(this, function() {
            this.fitLayout();
          })),
          aspect.after(newInput, "_autoCompleteText", lang.hitch(this, function() {
            this.fitLayout();
          })),

          // Repeat changes form children to be picked up by parents
          on(newInput.focusNode, "blur", lang.hitch(this, function() {
            this.onBlur();
          })),
          on(newInput.focusNode, "change", lang.hitch(this, function() {
            this.onChange();
          })),

          // Listen for key presses to handle navigation
          on(newInput.focusNode, "keydown", lang.hitch(this, function(evt) {
            this._onInputKeydown(newInput, evt);
          })),

          // Listen for tokenisation events from this input
          aspect.after(newInput, "tokenise", lang.hitch(this, function(tokenText, fullText, start, end) {
            this.tokenise(tokenText, fullText, start, end, newInput);
          }), true)
      );

      if (append) {
        this.inputWidgets.push(newInput);
        domConstruct.place(newInput.domNode, this.containerNode, "last");
      }

      return newInput;
    },

    // TODO(orlade): Refactor this to be called automatically somehow.
    measureToken: function(/*Token*/ token) {
      // summary:
      //      Calculates the width of the given token in the DOM to allow calculation of input sizes.
      //      This method must be called manually every time a token is inserted into the AutoTextarea.
      token.cacheStyle = domStyle.getComputedStyle(token.domNode);
      token.cacheWidth = domGeometry.getMarginBox(token.domNode, token.cacheStyle).w;
      this.tokensWidth += token.cacheWidth;
    },

    buildToken: function(id, text, /*Boolean|AutoInput*/ append, ref) {
      // summary:
      //      Construct a new token item with the given properties.
      //      The caller is required to store the reference and DOM node unless append is specified.
      // id: Integer
      //      The ID of the item backing the token.
      // text: String
      //      The text to display on the token.
      // append: Object
      //      Whether to append the new token to the input container automatically (for convenience).
      //      If simply true, the token will be appended to the end of the container.
      //      If an input widget, the token will be appended after it.
      //      Otherwise, the token will not be placed or stored, and the caller is responsible for it.
      // ref: Object
      //      The original reference object generated from the value of this textarea. Can be null if tokenised.
      var token = this.createToken(id, text, ref);

      token.own(
          aspect.after(token, 'onChange', lang.hitch(this, function() {
            this.onChange();
//                    this.fitLayout();
          })),
          aspect.before(token, 'remove', lang.hitch(this, function() {
            this.onTokenDelete(token);
          }))
      );

      // Append the end
      if (append === true) {
        this.tokens.push(token);
        domConstruct.place(token.domNode, this.containerNode, "last");
        this.measureToken(token);
      }
      // Assume append is an input widget
      else if (append) {
        var index = array.indexOf(this.inputWidgets, append);
        if (index !== -1) {
          this.tokens.splice(index + 1, 0, token);
          domConstruct.place(token.domNode, append.domNode, "after");
          this.measureToken(token);
        }
      }
      return token;
    },

    createToken: function(id, label, ref) {
      // summary:
      //      Constructs an instance of a Token.
      // id: Integer
      //      The ID of the item backing the token.
      // value: String
      //      The value to display on the token.
      // ref: Object
      //      The original reference object generated from the value of this textarea. Can be null if tokenised.
      return new Token({itemId: id, text: label});
    },

    tokenise: function(item, fullText, start, end, inputWidget) {
      // summary:
      //      Convert a completed item into an item widget.
      if (!item || !inputWidget) {
        return;
      }
      var inputIndex = array.indexOf(this.inputWidgets, inputWidget);

      // Split out a new input for the left of the token with any leading text
      var newInput = this.buildInput(fullText.substring(0, start));
      this.inputWidgets.splice(inputIndex, 0, newInput);
      domConstruct.place(newInput.domNode, inputWidget.domNode, "before");

      // Create and insert the token
      var token = this.buildToken(item.id, item.name, false);
      this.tokens.splice(inputIndex, 0, token);
      domConstruct.place(token.domNode, inputWidget.domNode, "before");
      this.measureToken(token);

      // Leave any trailing text in the original input
      inputWidget.set("value", inputWidget.get("value").substr(end));
      // FIXME Doesn't go to the start, don't know why
      this.focusInput(inputWidget, "start");
      // Validate the new content
      inputWidget.validate();
      // Call the hook
      this.onChange();
    },

    focusInput: function(input, index) {
      // summary:
      //      Convenience method for focusing on an input widget.
      // input:
      //      The input widget to focus on.
      // index:
      //      The numeric character index to place the caret before (from [0, length]).
      //      May also be one of the strings {"start", "end"}.
      input.focus();
      input.setCaretPos(index);
    },

    fitLayout: function() {
      // summary:
      //      Manually adjust the width of each child widget to fit seamlessly.
      //      Must be called after the list of children is correct (after create/remove).

      // The width of all elements on the current line so far.
      var cumulativeWidth = 0;
      var charWidth = 9;

      // If the container sizing hasn't completed properly yet, ignore the fit request.
      if (this.containerWidth < charWidth) {
        return;
      }

      var calculateTextWidth = function(/*String*/ text) {
        // summary:
        //      Calculates the pixel width of an input required to contain the given text.
        return text.length * charWidth + 1; // int
        // +1 for comfort/margin-right.
      };

      var setInputWidth = function(/*AutoInput*/ input, /*int*/ width) {
        // summary:
        //      Sets the given input element to the given width in pixels.
        domStyle.set(input.domNode, "width", Math.max(width, charWidth) + "px");
      };

      var fillInputWidth = lang.hitch(this, function(/*AutoInput*/ input, /*int*/ remainingWidth) {
        // summary:
        //      Sets the given input element width to fill the rest of the line.
        domClass.add(input.domNode, "autoboxLast");
        setInputWidth(input, remainingWidth);
      });

      var transferText = lang.hitch(this, function(/*AutoInput*/ from, /*AutoInput*/ to, /*int*/ start, /*int?*/ end, /*AutoInput?*/ focusInput, /*int?*/ focusIndex) {
        // summary:
        //      Transfers a substring from one end of the "from" input to the other end of the "to" input.
        //      If start = 0, the substring will be appended to the "to" input.
        //      If start > 0, the substring will be prepended to the "to" input.
        // from: AutoInput
        //      The input to remove text from.
        // to: AutoInput
        //      The input to add the removed text to.
        // start: Integer
        //      The index to start removing text from the "from" input.
        // end: Integer
        //      The index to stop removing text from the "from" input. If undefined, text will be removed until
        //      the end of the string.
        // focusInput: AutoInput
        //      The input to focus on after the transfer is complete (if any). After these sneaky operations, a
        //      manual update of the caret is typically required.
        // focusIndex: int
        //      The index on the focusInput to set the caret at after the operation is complete. If unspecified,
        //      It will be set back to wherever it was before the change.
        var text = from.get("value").substring(start, end);
        if (focusInput && (focusIndex === null || typeof focusIndex === "undefined")) {
          focusIndex = focusInput.getCaretPos();
        }
        if (start === 0) {
          from.set("value", from.get("value").substring(end));
          to.set("value", to.get("value") + text);
        } else {
          from.set("value", from.get("value").substring(0, start));
          to.set("value", text + to.get("value"));
        }
        if (focusInput) {
          this.focusInput(focusInput, focusIndex);
        }
      });

      var i = -1;
      while (this.inputWidgets[++i]) {
        var input = this.inputWidgets[i],
            nextInput = this.inputWidgets[i + 1],
            nextToken = this.tokens[i],
            nextTokenWidth = nextToken ? nextToken.cacheWidth + 1 : 0,
            remainingWidth = this.containerWidth - cumulativeWidth,
            requiredWidth = calculateTextWidth(input.get("value"));

        // If after the last adjustments another input won't fit, just start a new line.
        if (remainingWidth <= charWidth) {
          cumulativeWidth = 0;
          remainingWidth = this.containerWidth;
        }

        if (requiredWidth > remainingWidth) { // The input won't completely fit.
          // Split the content
          var split = Math.floor(remainingWidth / charWidth);
          if (nextToken || !nextInput) { // If there isn't a split input following this one, build it.
            nextInput = this.buildInput("", false);
            this.inputWidgets.splice(i + 1, 0, nextInput);
            domConstruct.place(nextInput.domNode, input.domNode, "after");
            this.tokens.splice(i, 0, null); // Create a null token to keep inputs and tokens aligned.
            nextToken = null;
          }
          if (input.getCaretPos() === input.get("value").length) {
            transferText(input, nextInput, split, undefined, nextInput, input.get("value").length - split);
          } else {
            transferText(input, nextInput, split, undefined, input);
          }

          // Fill the remaining space with the existing input.
          fillInputWidth(input, remainingWidth);
          cumulativeWidth = 0; // Start the next line.
          continue;
        }

        if (nextToken) { // Followed by a token, consider its width.
          if (requiredWidth + nextTokenWidth > remainingWidth) { // Input will fit, but the next token won't.
            // The input will fit, but the next token won't.
            fillInputWidth(input, remainingWidth);
            cumulativeWidth = nextTokenWidth;
          } else { // Input and token fit.
            domClass.remove(input.domNode, "autoboxLast");
            setInputWidth(input, requiredWidth);
            cumulativeWidth += requiredWidth + nextTokenWidth;
          }
        } else if (nextInput) { // Followed by a split input, consider merging.
          var spareLength = Math.floor(remainingWidth / charWidth) - input.get("value").length;
          if (spareLength > 0) { // There is space to merge some input back.
            // TODO(orlade): Pass the currently-focused input widget to avoid jumping from other widgets.
            transferText(nextInput, input, 0, spareLength, /*currentlyFocused*/input);
            if (!nextInput.get("value")) { // Fully merged.
              this.removeInput(nextInput);
              // Run the sizing over the same input again in case it has a nextToken now.
              i--;
              continue;
            }
          }
          // The split input couldn't be merged, take the remaining width.
          fillInputWidth(input, remainingWidth);
          cumulativeWidth = 0; // Start the new line for the next input.
        } else { // Last item in the field.
          fillInputWidth(input, remainingWidth);
        }
      }
    },

    removeInput: function(input, index) {
      // summary:
      //      Removes the input from the widget (reference and DOM node).
      // input:
      //      The input widget to remove
      // index:
      //      The index of the widget in this.inputWidgets, if known.
      if (index === null || typeof index === "undefined") {
        index = array.indexOf(this.inputWidgets, input);
      }
      input.destroy();
      this.inputWidgets.splice(index, 1);
    },

    onTokenDelete: function(token) {
      // summary:
      //      Handle the deletion of a token.

      // Remove the token
      console.debug('deleting token', token);
      this.tokensWidth -= token.cacheWidth;
      var tokenIndex = array.indexOf(this.tokens, token);
      this.tokens.splice(tokenIndex, 1);

      // Merge the (n+1)th input into the nth
      var left = this.inputWidgets[tokenIndex];
      var right = this.inputWidgets[tokenIndex + 1];
      // Save the left length for cursor placement
      var leftLength = left.get("value").length;
      // Merge the text
      left.set("value", left.get("value") + right.get("value"));
      // Remove the right input
      this.removeInput(right, tokenIndex + 1);
      // Update the focus to what it would have been
      this.focusInput(left, leftLength);
      console.debug('done', token);
    },

    reset: function(/*Boolean?*/ total) {
      // summary:
      //      Clear the auto-complete widget of all children except an empty input.
      // total:
      //      Destory the original input as well.

      total = total || false;

      // Remove all tokens
      for (var i = this.tokens.length - 1; i >= 0; i--) {
        this.tokens[i] && this.tokens[i].remove();
      }
      this.tokens = [];

      // Remove all inputs but the first (must be after tokens are removed)
      array.forEach(array.filter(this.inputWidgets, function() {
        return true;
      }), lang.hitch(this, function(input, i) {
        if (total || i > 0) {
          this.removeInput(input);
        }
      }));

      if (!total) {
        // Reset the first input
        this.inputWidgets = [this.inputWidgets[0]];
        this.inputWidgets[0].set("value", "");
        this.fitLayout();
      } else {
        this.inputWidgets = [];
      }
    },

//        tokenToString: function (token) {
//            // summary:
//            //      Method for customising the serialisation of tokens.
//            return this.itemPrefix + token.itemId;
//        },

//        stringToTokens: function (value) {
//            // summary:
//            //      Method for customising the parsing of a string to tokens.
//            return value.split(/\$\d+/);
//        },

    toString: function() {
      // summary:
      //      Sequentially serialise the contents of this autobox into a string.
      var output = "";
      if (this.inputWidgets.length) {
        output = this.inputWidgets[0].get("value");
        array.forEach(this.tokens, lang.hitch(this, function(token, i) {
          if (token) {
            output += token.get('value') + this.inputWidgets[i + 1].get("value");
          }
        }));
      }
      return output;
    },

    onBlur: function() {
      // summary:
      //      Hook to handle the event of blurring out of any input.
    },

    onChange: function() {
      // summary:
      //      Hook to handle the event of changing the content of any input.
    },

    findRefs: function(/*String*/ text, map) {
      // summary:
      //      Find all start-end index pairs representing references.
      // text:
      //      The text to search within.
      // map:
      //      A function to modify the references as they are added.
      // return:
      //      Array of {start, end, id} maps.

      map = typeof map != 'undefined' ? map : lang.hitch(this, this._refMatch);
      var refs = [];
      var match;
      while (match = this.itemRegex.exec(text)) {
        var ref = map ? map(match) : match;
        refs.push(ref);
      }
      return refs;
    },

    _refMatch: function(match) {
      var ref = {};
      ref.id = parseInt(match[1]);
      ref.start = match.index;
      ref.end = ref.start + match[0].length;
      return ref;
    },

    findRefsInStore: function(refs, callback) {
      // summary:
      //      Retrieves items from the store using reference ids.
      // refs: Array
      //      The references.
      // callback:
      //      A function to call after all items are retrieved.
      //      Arguments passed are the original refs and items arrays.
      var remaining = refs.length;
      var items = {};
      refs.forEach(lang.hitch(this, function(ref) {
        if (!(ref.id in items)) {
          // TODO must be int here
          when(this.store.get(ref.id), lang.hitch(this, function(item) {
            items[ref.id] = item;
            if (--remaining == 0) {
              // All items received from store
              callback(refs, items);
            }
          }));
        }
      }));
    },

    validate: function() {
      // summary:
      //      Determines whether the input value is valid or not. Currently very basic,
      //      incomplete syntax checking to help prevent basic errors from UI bugs.
      // returns:
      //      Falsey if the input is valid, or else an array of descriptive error messages.

      // TODO(orlade): Allow commas.
      var VALID_AFTER_TOKEN = '+-*/^)';
      var errors = [];

      this.inputWidgets.forEach(lang.hitch(this, function(input, index) {
        var value = input.get('value');

        // Check for illegal characters (basically all of them) in input.
        var letters = /([a-zA-Z,?!@#$%&_=|;:'"]+)/g;
        var result = letters.exec(value);
        if (result) {
          var realMatch = false;
          var error = 'Illegal characters in expression: ';
          var matchEnd = 0;
          do {
            var match = result[0];
            // TODO(orlade): Generalise the conditions under which characters are allowed.
            // TODO(orlade): Be more rigorous matching valid Java Math functions.
            if (match.substr(0, 4) !== 'Math') {
              error += value.substring(matchEnd, result.index) + '[' + match + ']';
              realMatch = true;
            } else {
              error += match;
            }
            matchEnd = letters.lastIndex;
          } while (result = letters.exec(value));
          if (realMatch) {
            error += value.substr(matchEnd);
            errors.push(error);
          }
        }

        // Check that the previous token (if any) is followed by a feasible operator.
        var trimmedValue = value.trim();
        var previousToken = this.tokens[index - 1];
        if (index >= 1 && previousToken) {
          if (trimmedValue && VALID_AFTER_TOKEN.indexOf(trimmedValue[0]) === -1) {
            var error = 'Expected operator after token: ' + previousToken.get('text') +
                '[?]' + input.get('value');
            if (index < this.inputWidgets.length - 1) error += '...';
            errors.push(error);
          }
        }
      }));
      return errors.length ? errors : false;
    },

    _getValueAttr: function() {
      // summary:
      //      Interface for widget.get("value").
      // returns:
      //      A stringified version of the input expression, or null if empty.
      return this.toString() || null;
    },

    _setValueAttr: function(value) {
      // summary:
      //      Set up the auto-complete widget given a serialised string.
      if (this.pending) {
        this.pending = value;
        return;
      }
      this.pending = value;

      // Clear any existing content and start from scratch
      // If there's no content, we're done
      if (value === undefined || value === null || value === '') {
        this.reset();
        return;
      }
      // If there is content, wipe out the original input as well
      else {
        value = value + ''; // Ensure the value is a string
        this.reset(true);
      }

      var refs = this.findRefs(value);

      // Request data from store before creating tokens
      // NOTE: otherwise we cannot guarantee tokens are created in correct order and this leads to tokens swapping order
      if (refs.length > 0) {
        this.findRefsInStore(refs, lang.hitch(this, function(refs, items) {
          this.__setValueAttr(value, refs, items);
        }));
      } else {
        this.__setValueAttr(value, [], []);
      }
    },

    __setValueAttr: function(value, refs, items) {
      var lastEnd = 0;
      refs.forEach(lang.hitch(this, function(ref) {
        var item = items[ref.id];
        // Create the left widget
        var newInput = this.buildInput(value.substring(lastEnd, ref.start), true);
        // Create the token
        this.buildToken(item.id, item.name, newInput, ref);
        // Keep track of where we're up to
        lastEnd = ref.end;
      }));
      // Add the final right-most input
      this.buildInput(value.substr(lastEnd), true);
      // After all is done, make sure everything fits
      this.fitLayout();

      // If another value was set in the meantime, process it now.
      if (this.pending !== this.get('value')) {
        this.set('value', this.pending);
      }
      this.pending = null;
    },

    _setStoreAttr: function(store) {
      this.store = store;
      array.forEach(this.inputWidgets, function(input) {
        input.set("store", store);
      });
    },

    _setQueryAttr: function(query) {
      this.query = query;
      array.forEach(this.inputWidgets, function(input) {
        input.set("query", query);
      });
    },

    _setPlaceholderAttr: function(text) {
      // summary:
      //      Allow the placeholder text to be set if there's only a single textbox.
      if (this.inputWidgets.length === 1) {
        this.inputWidgets[0].set("placeholder", text);
      }
    }
  });
});
