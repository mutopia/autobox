define([
  "dojo/_base/declare",
  "dojo/_base/lang",
  "dojo/on",
  "dojo/query",
  "dijit/_WidgetBase",
  "dijit/_TemplatedMixin",
  "dojo/text!./templates/Token.html"
], function(declare, lang, on, query, _WidgetBase, _TemplatedMixin, template) {
  return declare([_WidgetBase, _TemplatedMixin], {
    // summary:
    //      A simple element that represents some underlying item, and displays a label and a remove button. When
    //      serialized with token.get("value"), it generates a string with the ID of the item prefixed by a "$".
    //      Designed to be integrated with the AutoTextarea to display the autocompleted values.

    // templateString: String
    //      The template that defines the basic DOM structure of the token.
    templateString: template,

    // itemPrefix: String
    //      The character used as a prefix to designate an object ID when the token is serialized.
    itemPrefix: "$",

    /*=====
     // text: String
     //        The text to display on the token. This should be passed into the constructor.
     text: "",
     =====*/

    // itemId: Object
    //      The ID of the underlying item. This should be passed into the constructor.
    itemId: null,

    _getTextAttr: function() {
      // summary:
      //      Retrieves the text content of the token from the label element.
      // returns:
      //      The token label text.
      return this.label.textContent; // String
    },

    _setTextAttr: function(/*String*/ text) {
      // summary:
      //      Updates the token label text.
      this.label.textContent = text;
      this.onChange();
    },

    _setValueAttr: function(value) {
      // summary:
      //      Would be used to update the underlying item ID and label text, but this is not supported yet.
      throw "Setting token value is not supported.";
    },

    _getValueAttr: function() {
      // summary:
      //      Serializes the token into a string.
      return this.itemPrefix + this.itemId;
    },

    onChange: function() {
      // summary:
      //      A hook that should be called whenever the content of the token changes materially (i.e. text,
      //      buttons, general sizing). Other widgets can listen for calls to this hook and react accordingly.
    },

    remove: function() {
      // summary:
      //      Destroy the token widget.
      this.destroy(false);
    }
  });
  /* end declare() */
});
/* end define() */
