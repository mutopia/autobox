/**
 * This file determines build parameters for Autobox.
 */

var testRegex = /^autobox\/tests\/?/;

// Taken from dojo/dojo.profile.js.
var copyOnly = function (filename, mid) {
  var list = {
    "autobox/package.json": 1,
    "autobox/tests": 1
  };
  return (mid in list) ||
      /^autobox\/_base\/config\w+$/.test(mid) ||
      (/^autobox\/style\//.test(mid) && !/\.css$/.test(filename)) ||
      /(png|jpg|jpeg|gif|tiff)$/.test(filename) ||
      /built\-i18n\-test\/152\-build/.test(mid)
};

var profile = {
  resourceTags: {
    amd: function (filename, mid) {
      return !copyOnly(filename, mid) && /\.js$/.test(filename);
    },

    copyOnly: function (filename, mid) {
      return copyOnly(filename, mid);
    },

    // Use mini: true in profile to exclude test files in preference to the test tag and copyTests.
    miniExclude: function (filename, mid) {
      return mid === 'autobox/package' || testRegex.test(mid) || /index\.html$/.test(filename);
    },

    test: function (filename, mid) {
      return false;
    }
  }
};
